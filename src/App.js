import React from 'react';
import Router from './Router';

// main app wrapper
// add common for all pages components here ax. [header, footer]
function App() {
	return (
		<div className="App">
			<Router />
		</div>
	);
}

export default App;
