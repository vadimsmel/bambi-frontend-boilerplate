/*
	Api route call methods divided into categories.
*/

import axios from 'axios';
const baseUrl = 'https://bambi.free.beeceptor.com';

export default {
	app() {
		const url = baseUrl + 'url/';
		return {
			getData: () => {
				return axios({
					url: url,
					method: 'GET',
				});
			},
		};
	},
	weather() {
		return {
			getLondonWeather: () => {
				const url = baseUrl + '/weather/search/?query=London';
				return axios({
					url: url,
					method: 'GET',
				});
			},
		};
	},
};
