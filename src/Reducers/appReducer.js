import { REDUX_TEST } from '../Actions';
import produce from 'immer';

const initialState = {
	test: 'un-initiated',
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case REDUX_TEST.test: {
				draft.test = action.params;

				return draft;
			}

			default:
				return draft;
		}
	});
