import { GET_LONDON_WEATHER } from '../Actions';
import produce from 'immer';

const initialState = {
	london: '0 c',
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case GET_LONDON_WEATHER.success: {
				draft.london = action.weatherDetail;

				return draft;
			}

			default:
				return draft;
		}
	});
