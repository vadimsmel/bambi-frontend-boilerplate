import { combineReducers } from 'redux';
import AppReducer from './appReducer';
import WeatherReducer from './weatherReducer';

// inits all reducers
const reducers = combineReducers({
	app: AppReducer,
	weather: WeatherReducer,
});

export default reducers;
