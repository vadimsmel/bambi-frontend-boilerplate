import React, { useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GET_LONDON_WEATHER, ReduxTest } from '../Actions/index';
import { getReduxTest, getWeather } from '../Selectors';
import { WelcomePageElement } from './Styled/WelcomePageBoilerPlate';
import { useModal } from '../Contexts/Modals';
import ExampleModal from './Modals/ExampleModal';
import { Modal } from 'antd';

function WelcomePage() {
	const dispatch = useDispatch();
	const reduxTest = useSelector(getReduxTest);
	const londonWeather = useSelector(getWeather);
	const {
		exampleModal: { showExampleModal, toggleExampleModal },
	} = useModal();

	useLayoutEffect(() => {
		setTimeout(() => {
			dispatch({ type: GET_LONDON_WEATHER.request });
		}, 1500);
	}, []);

	function handleCancel() {
		console.info('Cancel');
		toggleExampleModal();
	}

	function handleSubmit() {
		console.info('Submit');
		toggleExampleModal();
	}

	return (
		<WelcomePageElement height={window.innerHeight}>
			<div className="content">
				<h1>Bambi Boilerplate</h1>
				<h3>Features:</h3>
				<ul>
					<li>
						<span>Redux</span> + <span>react-redux</span>
					</li>
					<li>
						<span>Reselect</span> for memoizing redux selectors
					</li>
					<li>
						<span>axios</span> for api calls
					</li>
					<li>
						<span>redux-saga</span> for side-effects
					</li>
					<li>
						<span>ant-design</span> / <span>material-ui</span> as ui template
					</li>
					<li>
						contains code formaters and linters to fixate on code styling and avoid errors
					</li>
				</ul>
				<h3>Rules:</h3>
				<ul>
					<li>
						Define style only with <span>styled-components</span>
					</li>
					<li>
						<span>Requests</span> has to flow through redux-saga via coresponding saga
						file
					</li>
					<li>
						<span>Responses</span> should get too the component through redux store
					</li>
					<li>
						<span>Reuse components</span> instead of creating new ones (think how to
						create them reusable from the beginning)
					</li>
					<li>
						<span>Create reselect selectors</span> to pull data to components
					</li>
					<li>
						<span>Only containers</span> should have access to redux
					</li>
					<li>
						Create as <span>simple components</span> as possible
					</li>
					<li>
						<span>Add comments</span> briefing about the component
					</li>
					<li>
						<span>modular/reusable</span> functions should be pulled out of component
						files to seperate helper file
					</li>
					<li>
						Use <span>Functional components</span> and <span>hooks</span> when state
						needed
					</li>
					<li>
						Only <span>Api</span> file can hold request functions
					</li>
					<li>
						<span>Modals</span> controlled only with context Api
					</li>
					<li>
						<span>Media folder</span> will contain all media assets
					</li>
				</ul>
				<div>
					<button onClick={() => toggleExampleModal()}>open example modal</button>
					<button onClick={() => dispatch(ReduxTest.test('passed'))}>
						trigger redux action
					</button>
					<p>test result: {reduxTest}</p>
				</div>
				<p>
					** Notice on refresh the value will keep being passed. that is due redux store
					being saved to the local storage.
				</p>
				<div>
					<h4>Async (Saga Example) Londons Weather is: </h4>
					<p>{londonWeather}</p>
				</div>
			</div>
			<Modal
				onCancel={handleCancel}
				onOk={handleSubmit}
				visible={showExampleModal}
				className="modal-base"
				title="Example Modal"
			>
				<ExampleModal />
			</Modal>
		</WelcomePageElement>
	);
}

export default WelcomePage;
