import styled from 'styled-components';

export const WelcomePageElement = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	background: #6666f5;
	min-height: ${props => props.height}px;
	font-family: monospace;

	.content {
		background: #fff;
		border-radius: 8px;
		padding: 20px 40px;
		width: 800px;
		height: auto;
		min-height: 50%;
		max-height: 90%;
		border: solid 1px lightgray;
		box-shadow: 1px 1px 30px rgba(0, 0, 0, 0.3);
		margin: 10px 0;

		h1 {
			text-align: center;
			margin-bottom: 40px;
		}

		h3,
		h4 {
			margin: 10px 0;
		}

		ul {
			list-style: square;

			li {
				margin: 5px 0 10px 20px;

				span {
					color: #6666f5;
					font-weight: 600;
					letter-spacing: 1px;
					font-size: 14px;
				}
			}
		}

		button {
			background: lightgray;
			border: solid 1px gray;
			padding: 5px 10px;
			border-radius: 4px;
			cursor: pointer;
		}

		div {
			display: flex;
			align-items: center;
			margin-bottom: 30px;
			margin-top: 20px;

			button + button {
				margin-left: 20px;
			}

			p {
				margin: 0;
				margin-left: 20px;
			}
		}
	}
`;
