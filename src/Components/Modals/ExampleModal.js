import React from 'react';
import PropTypes from 'prop-types';
import { ExampleModalElement } from '../Styled/ModalStyled';

const ExampleModal = () => {
	return (
		<ExampleModalElement className="modal-body">
			<span>Example modal</span>
		</ExampleModalElement>
	);
};

export default ExampleModal;

ExampleModal.propTypes = {
	onCancel: PropTypes.func,
	onSubmit: PropTypes.func,
	isLoading: PropTypes.bool,
};
