import React, { useState } from 'react';
const ModalContext = React.createContext();

const useModal = () => {
	const context = React.useContext(ModalContext);
	if (!context) {
		throw new Error('useModal must be used within a ModalProvider');
	}
	return context;
};

const ModalProvider = props => {
	const [showExampleModal, setExampleModal] = useState(false);

	const toggleExampleModal = () => setExampleModal(!showExampleModal);

	const modals = {
		exampleModal: { showExampleModal, toggleExampleModal },
	};

	return <ModalContext.Provider value={modals} {...props} />;
};

export { ModalProvider, useModal };
