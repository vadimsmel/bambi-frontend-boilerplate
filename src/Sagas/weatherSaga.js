import { call, put, takeLatest } from 'redux-saga/effects';
import Api from '../Utilities/Api';
import { GetLondonWeather, GET_LONDON_WEATHER } from '../Actions';

// sagas
export function* getLondonWeatherSaga() {
	try {
		const response = yield call(() => Api.weather().getLondonWeather());
		if (response.status === 200) {
			yield put(GetLondonWeather.success(response.data));
		} else {
			yield put(GetLondonWeather.failed('bad url!'));
		}
	} catch (error) {
		console.error(error);
		yield put(GetLondonWeather.fatal(error));
	}
}

// watchers
export function* weatherWatcher() {
	yield takeLatest(GET_LONDON_WEATHER.request, getLondonWeatherSaga);
	// console.info('*** weather Saga Watcher Inititated ***');
}
