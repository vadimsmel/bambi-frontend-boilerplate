// import { createSelector } from 'reselect';
// import get from 'lodash/get';

// App Selectors
export const getReduxTest = state => state.app.test;
export const getWeather = state => state.weather.london;

// export const CurrentClientStatusSelector = createSelector(
// 	[getter1, getter2],
// 	(data1, data2) => {
// 		const found = get(data1, [data2, 'value']);
// 		if (found) {
// 			return found;
// 		}
// 		return null;
// 	}
// );
