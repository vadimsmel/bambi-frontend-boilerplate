import React from 'react';
import WelcomePage from './Components/WelcomePage';
import {
	BrowserRouter as RouterElement,
	Switch,
	Route,
	Redirect,
} from 'react-router-dom';

// the main router of the app
function Router() {
	return (
		<RouterElement>
			<Switch>
				<Route path="/" component={WelcomePage} />
				<Route path="/help" component={WelcomePage} />
				<Redirect to="/" />
			</Switch>
		</RouterElement>
	);
}

export default Router;
