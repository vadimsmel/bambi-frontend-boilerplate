export const GET_LONDON_WEATHER = {
	request: 'GET_LONDON_WEATHER',
	success: 'GET_LONDON_WEATHER_SUCCESS',
	failed: 'GET_LONDON_WEATHER_FAILED',
	fatal: 'GET_LONDON_WEATHER_FATAL',
};

export const GetLondonWeather = {
	request: locationParams => ({
		type: GET_LONDON_WEATHER.request,
		locationParams,
	}),
	success: weatherDetail => ({
		type: GET_LONDON_WEATHER.success,
		weatherDetail,
	}),
	failed: error => ({
		type: GET_LONDON_WEATHER.failed,
		error,
	}),
	fatal: error => ({
		type: GET_LONDON_WEATHER.fatal,
		error,
	}),
};
