export const REDUX_TEST = {
	test: 'REDUX_TEST',
};

export const ReduxTest = {
	test: params => ({
		type: REDUX_TEST.test,
		params,
	}),
};
