import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './CSS/base.css';
import './CSS/error.css';
import 'ress';
import 'antd/dist/antd.css';
import App from './App';
import configureStore from './Config/configStore';
import ErrorBoundary from './Config/ErrorBoundary';
import { ModalProvider } from './Contexts/Modals';
const store = configureStore();

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<ErrorBoundary>
				<ModalProvider>
					<App />
				</ModalProvider>
			</ErrorBoundary>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);
