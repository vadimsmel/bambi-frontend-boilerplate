// methods to store and pull data from local storage
// will JSON parsing to keep every detail accurate

export const loadState = () => {
	try {
		const serializedState = sessionStorage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}
		return JSON.parse(serializedState);
	} catch (err) {
		console.error(err);
		return undefined;
	}
};

export const saveState = state => {
	try {
		const serializedState = JSON.stringify(state);
		sessionStorage.setItem('state', serializedState);
	} catch (err) {
		console.error(err);
	}
};
