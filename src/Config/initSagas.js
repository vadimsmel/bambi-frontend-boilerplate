import * as sagas from '../Sagas';

// collects all saga files and binds them into the project
export const initSagas = sagaMiddleware => {
	Object.values(sagas).forEach(sagaMiddleware.run.bind(sagaMiddleware));
};
