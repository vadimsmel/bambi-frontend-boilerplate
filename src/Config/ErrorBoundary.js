import React from 'react';
import PropTypes from 'prop-types';

// If no other exception handler catched the error it will fall here
class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);
		this.state = { error: null, errorInfo: null };
	}

	static getDerivedStateFromError(error) {
		// Update state so the next render will show the fallback UI.
		console.error(error);
		return { hasError: true };
	}

	componentDidCatch(error, errorInfo) {
		// You can also log the error to an error reporting service
		this.setState({
			error: error,
			errorInfo: errorInfo,
		});
		console.error(error, errorInfo);
	}

	render() {
		if (this.state.hasError) {
			return (
				<div className="error-boundary">
					<div className="wrapper">
						<h1>שגיאה</h1>
						<p>עמוד אליו אתם מנסים להגיע לא נמצא. אנא עברו לעמוד הבית ונסו שוב.</p>
						<div className="buttons">
							<a href="/">עמוד בית</a>
							<br />
							<span>מצטערים על אי הנוחות.</span>
						</div>
					</div>
				</div>
			);
		}
		return this.props.children;
	}
}

export default ErrorBoundary;

ErrorBoundary.propTypes = {
	children: PropTypes.object,
};
