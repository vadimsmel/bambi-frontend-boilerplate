/*
	Store initialization with reducers, middlewares and sagas
	Store is saved to the local storage
*/

import throttle from 'lodash/throttle';
import { loadState, saveState } from './localStorage';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from '../Reducers';
import { initSagas } from './initSagas';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistedState = loadState();

// you can pull and save only specific parts of the store here
const configureStore = () => {
	const store = createStore(
		reducers,
		persistedState,
		composeEnhancer(applyMiddleware(sagaMiddleware))
	);

	store.subscribe(
		throttle(() => {
			const state = store.getState();
			saveState(state);
		}),
		1000
	);

	initSagas(sagaMiddleware);
	return store;
};

export default configureStore;
