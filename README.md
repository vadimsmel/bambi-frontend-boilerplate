### Bambi Boilerplate

## Features:

- **Redux** + **react-redux**
- **Reselect** for memoizing redux selectors
- **axios** for api calls
- **redux-saga** for side-effects
- **ant-design** / **material-ui** as ui template
- contains code formaters and linters to fixate on code styling and avoid errors


## Rules:

- Define style only with **styled-components**
- **Requests** has to flow through redux-saga via coresponding saga file
- **Responses** should get too the component through redux store
- **Reuse components** instead of creating new ones (think how to create them reusable from the beginning)
- **Create reselect selectors** to pull data to components
- **Only containers** should have access to redux
- Create as **simple components** as possible
- **Add comments** briefing about the component
- modular/reusable functions should be pulled out of component files to seperate helper file
- Use **Functional components** and **hooks** when state needed
- Only **Api** file can hold request functions
- **Modals** controlled only with context Api
- **Media folder** will contain all media assets

## Scripts:

- start - starts the app for development
- build - creates a build for production
- lint - fixes all lint errors


**Note: No pollifils included, supports only modern browsers!**
